# Guide to contribute

## Merge a new feature

To merge a new feature, follow these steps:

1. Fork project
2. Clone forked project
3. Add following variables to CI Variables (Repo Settings -> CI/CD -> Variables)
    a. DHLOGIN: Login username to Docker Hub
    b. DHPASS: Login password for Docker Hub
    c. DHREPO: Short URL for Docker Repo location (e.g.: username/repo)
4. Set up a runner, check gitlab yaml file for the name.
5. Work with forked repository

## Keep your forked repo up to date

```
# Add original repository as upstream to remote
git remote add upstream https://gitlab.com/iotops/container-images/mattermost.git
# Fetch changes
git fetch upstream
# Merge changes to branch
git pull upstream/$BRANCH
```

## Request merge with our develop branch

1. Go to repo and select Merge Request
2. Create New merge request
3. Select source project and branch
4. Select target branch: `develop`
5. Compare branchs and send request


